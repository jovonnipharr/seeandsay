//
//  CardDisplay.h
//  SeeAndSay
//
//  Created by NewUser on 1/30/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardDisplay : UIViewController {
    
    IBOutlet UIImageView *flashcard;
    IBOutlet UILabel *word;
    

    
    //arrays
    NSMutableArray *the_words;
    NSMutableArray *the_images;
    
    NSString *theactiveflashcardimage;
    NSString *theactiveflashcardword;
    
    int index;
    
    NSString *resultText;
    
    BOOL didSpeak;
    
    NSTimer *didSpeakTimer;
    
    IBOutlet UIImageView *verdict;
    
    
}

-(void)showCard;
-(void)hideCard;
-(void)pushback;

-(void)getassets;

-(void)setassets;

-(void)recognize;

////test buttons

-(IBAction)showCard:(id)sender;
-(IBAction)hideCard:(id)sender;
-(IBAction)speaknow:(id)sender;


-(void)didSpeakTimer;
-(BOOL)isAnswerRight;

-(void)animateGood;

-(void)setRight;
-(void)setWrong;

-(IBAction)back:(id)sender;

    

//end test buttons



@end
