//
//  DataModel.h
//  SeeAndSay
//
//  Created by NewUser on 1/31/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataModel : NSObject {
    
    NSMutableArray *all_words;
    NSMutableArray *all_images;
    
    NSString *activeFlashCardImage;
    NSString *activeFlashCardWord;
    
    NSString *category;
    
}


@property (readwrite, retain) NSMutableArray *all_words;
@property (readwrite, retain) NSMutableArray *all_images;

//active flash card
@property (readwrite, retain) NSString *activeFlashCardImage;
@property (readwrite, retain) NSString *activeFlashCardWord;

//category
@property (readwrite, retain) NSString *category;

+ (id)sharedInstance;

@end
