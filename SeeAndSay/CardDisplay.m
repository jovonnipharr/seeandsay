//
//  CardDisplay.m
//  SeeAndSay
//
//  Created by NewUser on 1/30/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "CardDisplay.h"
#import "ISSpeechRecognition.h"
#import "DataModel.h"

@implementation CardDisplay

-(IBAction)back:(id)sender{
    
    [self dismissModalViewControllerAnimated:YES];
    
}

-(IBAction)showCard:(id)sender{
    
    [self showCard];
    
    
}
-(IBAction)hideCard:(id)sender{
    
    [self hideCard];
    
    
}

/////end test buttons

-(void)showCard {
    
    [self pushback];

    didSpeak = NO;
    
    //initiate did speak timer
    didSpeakTimer = [NSTimer scheduledTimerWithTimeInterval: 0.1
                                                     target:self
                                                   selector:@selector(didSpeakTimer)
                                                   userInfo:nil repeats:YES];
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationCurve:50];
	[UIView setAnimationDuration:1.0];
	
	CGPoint newcenter = [flashcard center];
	
	newcenter.x = 240;
	
	[flashcard setCenter:newcenter];
	
	[flashcard transform];
	
	
	/*
     CGRect rect = [myimage frame];
     rect.size.width *= .5;
     rect.size.height *= .5;
	 */
	
	
	//[myimage setFrame:rect];
    
    
	[flashcard setAlpha:1];
	
	[UIView commitAnimations];

    
    
    
}

-(void)hideCard {
    
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationCurve:50];
	[UIView setAnimationDuration:1.0];
    
    [UIView setAnimationDelegate:self];
    
    [UIView setAnimationDidStopSelector:@selector(getassets)];

	
	CGPoint newcenter = [flashcard center];
	
	newcenter.x = -240;
	
	[flashcard setCenter:newcenter];
	
	//[flashcard transform];
	
	
	/*
     CGRect rect = [myimage frame];
     rect.size.width *= .5;
     rect.size.height *= .5;
	 */
	
	
	//[myimage setFrame:rect];
    
    
	[flashcard setAlpha:0];
	
	[UIView commitAnimations];
    
    //send to other side
    
    
        
}

-(void)pushback{
    
    [flashcard setAlpha:0];
    
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationCurve:50];
	[UIView setAnimationDuration:0.1];
	
	CGPoint newcenter = [flashcard center];
	
	newcenter.x = 500;
	
	[flashcard setCenter:newcenter];
	
	[flashcard transform];
	
	
	/*
     CGRect rect = [myimage frame];
     rect.size.width *= .5;
     rect.size.height *= .5;
	 */
	
	
	//[myimage setFrame:rect];
    
    
	
	
	[UIView commitAnimations];

    
}

-(void)getassets{
    
    
    /////all words
    
    //intro datamodel
    DataModel *datamodel = [DataModel sharedInstance];
    
    //random word
    index = arc4random() % [the_words count];
    
    NSLog(@"INDEX: %i", index);
    
    //select word/image
    theactiveflashcardword = [the_words objectAtIndex:index];
    theactiveflashcardimage = [the_images objectAtIndex:index];
    
    ////end all words
    
    //set image/word globally
    
    datamodel.activeFlashCardImage = theactiveflashcardimage;
    
    datamodel.activeFlashCardWord = theactiveflashcardword;
    
    NSLog(@"Image: %@, Word: %@", theactiveflashcardimage,theactiveflashcardword);
    
    
    [self setassets];
    
}


-(void)setassets{
    
    NSLog(@"Setting assets...");
    
    [flashcard setImage:[UIImage imageNamed:[the_images objectAtIndex:index]]];
    
    [self showCard];
    
    //intro datamodel
    DataModel *datamodel = [DataModel sharedInstance];
    
    [word setText:datamodel.activeFlashCardWord];
    
}

-(IBAction)speaknow:(id)sender{
    
    [self recognize];
    
}



///////////////////// speech recognition

- (void)recognize {
	ISSpeechRecognition *recognition = [[ISSpeechRecognition alloc] init];
	
	NSError *err;
	
	[recognition setDelegate:self];
    
    NSLog(@"Yo");
	
	if(![recognition listen:&err]) {
		NSLog(@"ERROR: %@", err);
	}
}


- (void)recognition:(ISSpeechRecognition *)speechRecognition didGetRecognitionResult:(ISSpeechRecognitionResult *)result {
    //[self doSomethingWith:result];
    
    
    NSLog(@"Result: %@", result.text);
    
    resultText = result.text;
    
    didSpeak = YES;

}

-(void)didSpeakTimer{
    
    if(didSpeak == NO){
        
        //NSLog(@"didnt speak yet...");
        
    } else {
        
        //NSLog(@"Spoke Already...");
        [didSpeakTimer invalidate];
        didSpeakTimer = nil;
        [self isAnswerRight];
        
    }
    
    
    
}
 
-(BOOL)isAnswerRight{
    
    //intro datamodel
    DataModel *datamodel = [DataModel sharedInstance];
    
    //if(resultText == [datamodel.activeFlashCardWord lowercaseString]){
    
    if ([resultText isEqualToString:[datamodel.activeFlashCardWord lowercaseString]]) {
        
        NSLog(@"R: Say: %@ word: %@", resultText, [datamodel.activeFlashCardWord lowercaseString]);
        [self setRight];
        [self animateGood];
        return YES;
        
    } else {
        
        NSLog(@"W: Say: %@ word: %@", resultText, [datamodel.activeFlashCardWord lowercaseString]);
        [self setWrong];
        [self animateGood];
        return NO;
        
    }
    
}

//////////////////// end speech recognition




//right and wrong

-(void)animateGood{
    
    
    [verdict setAlpha:1];
    
    
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationCurve:50];
    [UIView setAnimationDelay:2];
	[UIView setAnimationDuration:1];
    [UIView setAnimationDelegate:self];
    
    [UIView setAnimationDidStopSelector:@selector(hideCard)];
	
	CGPoint newcenter = [verdict center];
	
    
	newcenter.y = 140;
	
	[verdict setCenter:newcenter];
	
	//[verdict transform];
    
    newcenter.y = -100;
	
	[verdict setCenter:newcenter];
	
	[verdict transform];
    
    [verdict setAlpha:0];
	
	
	[UIView commitAnimations];
    
    

}

-(void)setRight{
    
    [verdict setImage:[UIImage imageNamed:@"verdict_right.png"]];
    
}
-(void)setWrong{
    
    [verdict setImage:[UIImage imageNamed:@"verdict_wrong.png"]];

}





- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //init arrays
    
    
    the_words = [[NSMutableArray alloc] init];
    the_images = [[NSMutableArray alloc] init];
    
    
    
    
    
    //add words to array 
    [the_words addObject:@"Car"];
    [the_words addObject:@"Apple"];
    [the_words addObject:@"House"];
    
    [the_images addObject:@"car.png"];
    [the_images addObject:@"apple.png"];
    [the_images addObject:@"house.png"];
    
    NSLog(@"The Array: %@", the_words);
    
    
    

    
    [self getassets];
    
    //add images to array
    
    
    
    
    
    ///access server and get all words
    
    
    // access server and get all images
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
